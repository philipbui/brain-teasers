# brain-teasers

Find patterns, approach the problem one step at a time, and be prepared to throw away all common sense out the window.

**In a country in which people only want boys, every family continues to have children until they have a boy. If they have a girl, they have another child. If they have a boy, they stop. What is the proportion of boys to girls in the country?**

**How many times does a clock's hand overlap?**

- Twice every 60 seconds. (one for second -> minute, second -> hour) - 1 (where minute and hour hand already overlap)
- Once every 60 minutes. (one for minute -> hour) - 1 (where second, minute and hour hand already overlap) 
- Multiply by 2 for AM - PM.

**You're the captain of a pirate ship, and your crew gets to vote on how the gold is divided up. If fewer than half of the pirates agree with you, you die. How do you recommend apportioning the gold in such a way that you get a good share of the booty, but still survive?**

- Divide the gold equally to >=half the pirates is 100% safe and the most returning method.

**You have eight balls all of the same size, 7 of them weigh the same, and one of them weighs slightly more. How can you find the ball that is heavier by using a balance and only two weighings?**

- Measure 3 - 3 balls.
  - If equal, use last weigh to find greater of last 2 balls.
  - If not equal
    - Choose 2 balls and use last weigh on them.
      - If equal, unchosen 3rd ball is the heavier ball.

**You are given 2 eggs, you have access to a 100-story building. Eggs can be very hard or very fragile means it may break if dropped from the first floor or may not even break if dropped from 100th floor. Both eggs are identical. You need to figure out the highest floor of a 100-story building an egg can be dropped without breaking. The question is how many drops you need to make. You are allowed to break 2 eggs in the process.**

- Need to partition floors so an egg drop gives an answer range. E.g dropping at 10th shows the optimal is 0 - 9th, and 20th = 11th - 19th etc.
- Balance the answer range after each partition to still get the minimum egg drops required.
- 14 -> 27 ->39 -> 50 -> 60 -> 69 -> 77 -> 84 -> 90 -> 95
- Maximum is 14
  - 14 broken. 1 -> 1 - 13 = 14
  - 27 broken. 2 -> 15 - 26 = 14
  - 39 broken. 3 -> 28 - 38 = 14
  - 50 broken. 4 -> 40 - 50 = 14
  - 60 broken. 5 -> 51 - 60 = 14
  - 69 broken. 6 -> 61 - 68 = 14
  - 77 broken. 7 -> 70 - 76 = 14
  - 84 broken. 8 -> 78 - 83 = 14
  - 90 broken. 9 -> 85 - 89 = 14
  - 95 broken. 10 -> 91 - 94 = 14
  - 95 not broken. 10 -> 96 - 100 = 14
  
  

